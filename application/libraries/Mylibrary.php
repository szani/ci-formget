<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
 /*
 Könytárat 3 féle módon tudunk létrehozni: 
 
 1. application/libraries könyvtárba
 
 meghívása
 $this->load->library(‘mylibrary’); 
 $this->mylibrary->some_function();
 
 2. megkévő könyvtár kiterjesztése:
 
 Class MY_Email extends CI_Email { 
}

3. Meglévő könyvtár áthelyezése:
Email.php-fájlba

Class CI_Email { 
}

 */
 
 
   
   class Mylibrary {
	
      public function some_function() {
      }
   }
	
/* End of file Mylibrary.php */