<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

/*A controllert meghívhatjuk a következő 3 mód egyikén: 

* 
1. Controller neve - ilyenkor a default metódus hívódik meg:
http://www.your-domain.com/index.php/test
2. index metódus neve a controller név után
http://www.your-domain.com/index.php/test/index
3. hello metódus neve a controller neve után
http://www.your-domain.com/index.php/test/hello

*/

   class Test extends CI_Controller {  
       
      
	
      public function index() { 
      // view betöltése: $this->load->view('name'); $this->load->view('directory-name/name');
      //https://codeigniter-szani.c9users.io/ci-formget/index.php/Test/
   
         $this->load->helper('url'); 
       
         $this->load->view('test');
     //model User_model osztály meghívása
         $this->load->model('User_model');
         // további hivatkozása: $this->User_model->method();
         
         //profilozás engedélyezése
    
         $this->output->enable_profiler(TRUE);
         
         /*profilozás tiltása
         
         $this->output->enable_profiler(FALSE);
         */
      } 
  
      public function hello() { 
         echo "This is hello function."; 
      } 
      
      
   } 
   
?>

