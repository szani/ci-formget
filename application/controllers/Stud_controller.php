<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
//CRUD példa:
   class Stud_controller extends CI_Controller {
	
      function __construct() { 
         parent::__construct(); 
         $this->load->helper('url'); 
    //adatbázis betöltése
         $this->load->database(); 
      } 
// READ
    /*minden rekord betöltése az adatbázisból
     szintaxis: get([$table = ''[, $limit = NULL[, $offset = NULL]]])
     
         $table (string) − The table to query array

         $limit (int) − The LIMIT clause

         $offset (int) − The OFFSET clause
         
         Returns: CI_DB_result instance (method chaining)
         
         $query = $this->db->get("stud"); 
         $data['records'] = $query->result();
     
     */
  
      public function index() { 
         $query = $this->db->get("stud"); 
         $data['records'] = $query->result(); 
			
         $this->load->helper('url'); 
         $this->load->view('Stud_view',$data); 
      } 
      
// CREAT
   /*
   szintaxis: insert([$table = ''[, $set = NULL[, $escape = NULL]]])
   
         $table (string) − Table name

         $set (array) − An associative array of field/value pairs

         $escape (bool) − Whether to escape values and identifiers
         
         Returns: TRUE on success, FALSE on failure
         
         $data = array( 
         'roll_no' => ‘1’, 
         'name' => ‘Virat’ 
         ); 

         $this->db->insert("stud", $data);
   */
      public function add_student_view() { 
         $this->load->helper('form'); 
         $this->load->view('Stud_add'); 
      } 
  
      public function add_student() { 
         $this->load->model('Stud_Model');
			
         $data = array( 
            'roll_no' => $this->input->post('roll_no'), 
            'name' => $this->input->post('name') 
         ); 
			
         $this->Stud_Model->insert($data); 
   
         $query = $this->db->get("stud"); 
         $data['records'] = $query->result(); 
         $this->load->view('Stud_view',$data); 
      } 
  
//UPDATE 

    /*szintaxis: set($key[, $value = ''[, $escape = NULL]])
    
         $key (mixed) − Field name, or an array of field/value pairs

         $value (string) − Field value, if $key is a single field

         $escape (bool) − Whether to escape values and identifiers
    
         
         Returns: CI_DB_query_builder instance (method chaining)
     
   where() 
     
      szintaxis:  where($key[, $value = NULL[, $escape = NULL]])
      
         $key (mixed) − Name of field to compare, or associative array

         $value (mixed) − If a single key, compared to this value

         $escape (bool) − Whether to escape values and identifiers
     
         Returns: DB_query_builder instance
         
         
         
    update()
    
       szintaxis: update([$table = ''[, $set = NULL[, $where = NULL[, $limit = NULL]]]])
       
         $table (string) − Table name

         $set (array) − An associative array of field/value pairs

         $where (string) − The WHERE clause

         $limit (int) − The LIMIT clause
         
         
         Returns: TRUE on success, FALSE on failure
         
         $data = array( 
         'roll_no' => ‘1’, 
         'name' => ‘Virat’ 
         ); 
         
         $this->db->set($data); 
         $this->db->where("roll_no", ‘1’); 
         $this->db->update("stud", $data); 
         
         */
       
        public function update_student_view() { 
         $this->load->helper('form'); 
         $roll_no = $this->uri->segment('3'); 
         $query = $this->db->get_where("stud",array("roll_no"=>$roll_no));
         $data['records'] = $query->result(); 
         $data['old_roll_no'] = $roll_no; 
         $this->load->view('Stud_edit',$data); 
      } 
  
      public function update_student(){ 
         $this->load->model('Stud_Model');
			
         $data = array( 
            'roll_no' => $this->input->post('roll_no'), 
            'name' => $this->input->post('name') 
         ); 
			
         $old_roll_no = $this->input->post('old_roll_no'); 
         $this->Stud_Model->update($data,$old_roll_no); 
			
         $query = $this->db->get("stud"); 
         $data['records'] = $query->result(); 
         $this->load->view('Stud_view',$data); 
      } 
  
// DELETE 

    /*szintaxis: delete([$table = ''[, $where = ''[, $limit = NULL[, $reset_data = TRUE]]]])
    
         $table (mixed) − The table(s) to delete from; string or array

         $where (string) − The WHERE clause

         $limit (int) − The LIMIT clause

         $reset_data (bool) − TRUE to reset the query “write” clause
       
         Returns: CI_DB_query_builder instance (method chaining) or FALSE on failure
         
         $this->db->delete("stud", "roll_no = 1");
         
      */
       
        public function delete_student() { 
         $this->load->model('Stud_Model'); 
         /*
         $this->uri->segment(n); // n=1 for controller, n=2 for method, etc
         consider this example http://example.com/index.php/controller/action/1stsegment/2ndsegment
         
         it will return
         
         $this->uri->segment(1); // controller
         $this->uri->segment(2); // action
         $this->uri->segment(3); // 1stsegment
         $this->uri->segment(4); // 2ndsegment
                  
         */
         $roll_no = $this->uri->segment('3'); 
         $this->Stud_Model->delete($roll_no); 
   
         $query = $this->db->get("stud"); 
         $data['records'] = $query->result(); 
         $this->load->view('Stud_view',$data); 
      } 
      
       
   };
?>
