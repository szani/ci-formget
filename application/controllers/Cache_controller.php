<?php 
   class Cache_controller extends CI_Controller { 
      
      // application/cache könytárba készyt egy cache fájlt
	
      public function index() { 
         $this->output->cache(1); 
         $this->load->view('test'); 
      }
		
		//törli a cache fájlt
		
      public function delete_file_cache() { 
         $this->output->delete_cache('cachecontroller'); 
      } 
   } 
?>