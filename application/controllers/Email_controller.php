<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
   class Email_controller extends CI_Controller { 
 
      function __construct() { 
         parent::__construct(); 
         $this->load->library('session'); 
         $this->load->helper('form'); 
      } 
		
      public function index() { 
	
         $this->load->helper('form'); 
         $this->load->view('Email_form'); 
      } 
  
           /* példa:
           
           $this->email->from('your@example.com', 'Your Name');
            $this->email->to('someone@example.com');
             
            $this->email->subject('Email Test');
            $this->email->message('Testing the email class.');
            After that, execute the send() function as shown below to send an email.
            $this->email->send();
         */
      public function send_mail() { 
         
         $from_email = "szaniszlo.ivor@gmail.com"; 
         $to_email = $this->input->post('email'); 
   
         //Load email library 
         $this->load->library('email'); 
   
         $this->email->from($from_email, 'Your Name'); 
         $this->email->to($to_email);
         $this->email->subject('Email Test'); 
         $this->email->message('Testing the email class.'); 
   
         //Send mail 
         if($this->email->send()) 
         $this->session->set_flashdata("email_sent","Email sent successfully."); 
         else 
         $this->session->set_flashdata("email_sent","Error in sending Email."); 
         $this->load->view('Email_form'); 
      } 
   } 
?>